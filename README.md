# Projet IW3

## Features 
- Entity (Relation)
- CRUD (auto generated)
- Translations
- Structure Back & Front
- Embed Twig
- Form
- Assert (User Entity)
- AutoWiring
- Gedmo (sluggable, timestampable, sortable)
- Fixtures
- Webpack Encore 

## Installation
``docker-compose up -d``

``compose install``

``yarn install``

``yarn dev``

``yarn watch``

Vous pouvez vous connecter sur le site : http://localhost:8080/
