<?php

namespace App\Tests\UnitTest;

use App\Entity\Enfant;
use App\Entity\Personne;
use App\Entity\Evenement;
use App\Service\MailerService;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UnitTest extends KernelTestCase
{
    
    private $enfant;
    private $structure;
    private $personne;

    protected function setUp()
    {
        $enfant = new Enfant();
        $enfant->setBirthday(new \Datetime('1996/06/29'));

        $events = new Evenement();
        $events->setStartDate(new \Datetime('2020/07/16'));
        $events->setEndDate(new \Datetime('2020/07/17'));
        $faker = \Faker\Factory::create('fr_FR');

        $personne = new Personne();
        $personne->setLastname($faker->lastName)
        ->setFirstname($faker->firstName)
        ->setBirthday($faker->dateTimeBetween('-50 years', '-25 years'))
        ->setTel($faker->phoneNumber)
        ->setEmail($faker->safeEmail)
        ->setPassword('123AZEaze')
        ->setGender($faker->randomElement(['a', 'f', 'm']))
        ->setAddress($faker->streetAddress)
        ->setPostalcode($faker->postcode)
        ->setCity($faker->city)
        ->setAccountconfirmation(true);

        $this->personne = $personne;
        $this->events = $events;
        $this->enfant = $enfant;
    }

    public function testGetAge()
    {
        $this->assertEquals('24 ans', $this->enfant->getAge());
    }

    public function testEventDate()
    {
        $this->assertTrue($this->events->checkDates($this->events->getStartDate(), $this->events->getEndDate()));
    }

    public function testEventDateF()
    {
        $this->assertFalse($this->events->checkDates($this->events->getEndDate(), $this->events->getStartDate()));
    }

    public function testSendMail()
    {
        $mailerInterface = $this->createMock(MailerInterface::class);
        $mailerInterface->expects($this->any())
            ->method('send');

        $mailer = new MailerService($mailerInterface);

        $this->assertTrue($mailer->sendConfimationToken('kkidszen@gmail.com', 'TEST MAIL', null, $this->personne, 'structureCandidature.html.twig'));
    }
}
