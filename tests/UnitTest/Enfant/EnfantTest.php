<?php

namespace App\Tests\Enfant;

use App\Entity\Enfant;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class EnfantTest extends KernelTestCase
{
    private $enfant;
    private $structure;

    protected function setUp()
    {
        $enfant = new Enfant();
        $enfant->setBirthday(new \Datetime('1996/06/29'));

        $this->enfant = $enfant;
    }

    public function testGetAge()
    {
        $this->assertEquals('24 ans', $this->enfant->getAge());
    }
}
