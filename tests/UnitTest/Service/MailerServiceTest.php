<?php

namespace App\Tests\Service\MailerServiceTest;

use App\Entity\Personne;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Mailer\MailerInterface;

class MailerServiceTest extends KernelTestCase
{
    private $personne;

    public function setUp()
    {
        $faker = \Faker\Factory::create('fr_FR');

        $personne = new Personne();
        $personne->setLastname($faker->lastName)
        ->setFirstname($faker->firstName)
        ->setBirthday($faker->dateTimeBetween('-50 years', '-25 years'))
        ->setTel($faker->phoneNumber)
        ->setEmail($faker->safeEmail)
        ->setPassword('123AZEaze')
        ->setGender($faker->randomElement(['a', 'f', 'm']))
        ->setAddress($faker->streetAddress)
        ->setPostalcode($faker->postcode)
        ->setCity($faker->city)
        ->setAccountconfirmation(true);

        $this->personne = $personne;
    }

    public function testSendMail()
    {
        $mailerInterface = $this->createMock(MailerInterface::class);
        $mailerInterface->expects($this->any())
            ->method('send');

        $mailer = new MailerService($mailerInterface);

        $this->assertTrue($mailer->sendConfimationToken('kkidszen@gmail.com', 'TEST MAIL', null, $this->personne, 'structureCandidature.html.twig'));
    }
}
