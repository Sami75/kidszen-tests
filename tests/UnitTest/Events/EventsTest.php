<?php

namespace App\Tests\Events;

use App\Entity\Evenement;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class EventsTest extends KernelTestCase
{
    private $events;

    protected function setUp()
    {
        $events = new Evenement();
        $events->setStartDate(new \Datetime('2020/07/16'));
        $events->setEndDate(new \Datetime('2020/07/17'));

        $this->events = $events;
    }

    public function testEventDate()
    {
        $this->assertTrue($this->events->checkDates($this->events->getStartDate(), $this->events->getEndDate()));
    }

    public function testEventDateF()
    {
        $this->assertFalse($this->events->checkDates($this->events->getEndDate(), $this->events->getStartDate()));
    }
}
