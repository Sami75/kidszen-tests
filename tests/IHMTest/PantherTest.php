<?php

namespace App\Tests\PantherTest;

use Symfony\Component\Panther\PantherTestCase;

class PantherTest extends PantherTestCase
{
    private $client;

    protected function setUp()
    {
        $this->client = static::createPantherClient();
        $this->client->manage()->window()->maximize();
    }

    public function testPanelFamily()
    {
        $crawler = $this->client->request('GET', '/fr/inscription');
        sleep(4);

        $this->assertSelectorTextContains('#team_kidszen', 'Vous ne pouvez pas accéder à votre compte?');

        $btnSignUpCrawler = $crawler->selectButton('S\'inscrire');
        sleep(4);

        $form = $btnSignUpCrawler->form([
            'registration[lastname]' => 'Bruh',
            'registration[firstname]' => 'bruh',
            'registration[email]' => 'sami.bruh.sofiane2@mail.com',
            'registration[plainPassword][first]' => 'T46kp!JZt3',
            'registration[plainPassword][second]' => 'T46kp!JZt3',
        ]);

        $loginCrawler = $this->client->submit($form);

        sleep(5);
        // $this->assertSelectorTextContains('.f-16', 'Se connecter et explorer l\'univers Kidszen.');

        $btnLogin = $loginCrawler->selectButton('Se connecter');

        $form = $btnLogin->form([
            'email' => 'dias.bernard@example.org',
            'password' => '123AZEaze',
        ]);

        $familyCrawler = $this->client->submit($form);

        $this->assertSelectorTextContains('#profil-id', 'Profil');

        $link = $familyCrawler->selectLink('Structures')->link();
        $structureSCrawler = $this->client->click($link);
        sleep(5);

        $this->assertSelectorTextContains('.dt-page__title', 'Liste des structures');

        $link = $structureSCrawler->selectLink('En savoir plus / Inscrire un enfant')->link();
        $structCrawler = $this->client->click($link);
        sleep(3);

        $this->assertSelectorTextContains('.dt-page__title', 'Enfant');

        $btnApplication = $structCrawler->selectButton('Enregistrer un enfant');

        $form = $btnApplication->form([
            'enfant' => '40',
        ]);

        $familyCrawler2 = $this->client->submit($form);

        sleep(3);

        $link = $familyCrawler2->selectLink('Enfants')->link();
        $this->client->click($link);
        sleep(5);

        $link = $familyCrawler2->selectLink('Ajouter un enfant')->link();
        $addKidCrawler = $this->client->click($link);
        sleep(5);

        $link = $addKidCrawler->selectLink('account_circle')->link();
        $craw = $this->client->click($link);
        sleep(3);

        $link = $craw->selectLink('Déconnexion')->link();
        $this->client->click($link);
        sleep(5);
        $this->assertSelectorTextContains(' .f-16', 'Se connecter et explorer l\'univers Kidszen.');
    }
}
