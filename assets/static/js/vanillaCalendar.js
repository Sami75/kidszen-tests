var vanillacalendar = {
    month: document.querySelectorAll('[data-calendar-area="month"]')[0],
    next: document.querySelectorAll('[data-calendar-toggle="next"]')[0],
    previous: document.querySelectorAll('[data-calendar-toggle="previous"]')[0],
    label: document.querySelectorAll('[data-calendar-label="month"]')[0],
    activeDates: null,
    date: new Date(),
    todaysDate: new Date(),
  
    init: function () {
      this.date.setDate(1)
      this.createMonth()
      this.createListeners()
    },
  
    createListeners: function () {
      var _this = this
      this.next.addEventListener('click', function () {
        _this.clearCalendar()
        var nextMonth = _this.date.getMonth() + 1
        _this.date.setMonth(nextMonth)
        _this.createMonth()
      })
      // Clears the calendar and shows the previous month
      this.previous.addEventListener('click', function () {
        _this.clearCalendar()
        var prevMonth = _this.date.getMonth() - 1
        _this.date.setMonth(prevMonth)
        _this.createMonth()
      })
    },
  
    createDay: function (num, day, year) {
      var newDay = document.createElement('div')
      var dateEl = document.createElement('span')
      dateEl.innerHTML = num
      newDay.className = 'cal__date'
      newDay.setAttribute('data-calendar-date', this.date)
  
      if (num === 1) {
        var offset = ((day - 1) * 14.28)
        if (offset > 0) {
          newDay.style.marginLeft = offset + '%'
        }
      }
  
      // if (this.date.getTime() <= this.todaysDate.getTime() - 1) {
      //   newDay.classList.add('cal__date--disabled')
      // } else {
        newDay.classList.add('cal__date--active')
        newDay.setAttribute('data-calendar-status', 'active')
      // }
  
      if (this.date.toString() === this.todaysDate.toString()) {
        newDay.classList.add('cal__date--today')
      }
  
      newDay.appendChild(dateEl)
      this.month.appendChild(newDay)
    },
  
    dateClicked: function () {
      var _this = this
      this.activeDates = document.querySelectorAll('[data-calendar-status="active"]')
      for (var i = 0; i < this.activeDates.length; i++) {
        this.activeDates[i].addEventListener('click', function (event) {
          // var picked = document.querySelectorAll('[data-calendar-label="picked"]')[0]
          // picked.innerHTML = new Date(this.dataset.calendarDate).getDate()

            let date = new Date(this.dataset.calendarDate);

            let year    = date.getFullYear();
            let month   = date.getMonth() < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);
            let day     = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();

            let format  = year + '-' + month + '-' + day;

            $('.results').html('');

            $.ajax({
                method: "POST",
                url: lienAjax,
                data: {date: format},
                success: function(data){
                    let json = JSON.parse(JSON.stringify(data));

                    if(json.length === 0)
                        return;

                    for (let i = 0; i < json.length; i++)
                    {
                        let label       = json[i].label;
                        let description = json[i].description;
                        let startdate   = new Date(json[i].startdate.date);
                        let enddate     = new Date(json[i].enddate.date);
                        let type        = json[i].type;

                        let dayStart    = (startdate.getDate() + 1);
                        let monthStart  = startdate.getMonth();
                        let yearStart   = startdate.getFullYear();

                        let dayEnd      = (enddate.getDate() + 1);
                        let monthEnd    = enddate.getMonth();
                        let yearEnd     = enddate.getFullYear();

                        if(dayEnd < 10)
                            dayEnd = '0' + dayEnd;

                        if(dayStart < 10)
                            dayStart = '0' + dayStart;

                        if(monthEnd < 10)
                            monthEnd = '0' + monthEnd;

                        if(monthStart < 10)
                            monthStart = '0' + monthStart;

                        let formatStart = yearStart + '-' + monthStart + '-' + dayStart;
                        let formatEnd   = yearEnd + '-' + monthEnd + '-' + dayEnd;

                        $('.results').append('<div class="card shadow-none horizontal rounded-0 pb-8 border-bottom">\n' +
                            '                        <div class="dt-slider rounded-xl overflow-hidden">\n' +
                            '                            <img class="img-fluid" src="https://via.placeholder.com/250x150 " alt="Sunset">\n' +
                            '                        </div>\n' +
                            '                        <div class="card-stacked">\n' +
                            '                            <div class="card-body py-sm-0 px-0 px-sm-6 px-md-8">\n' +
                            '                                <span class="badge badge-secondary text-uppercase mb-2">' + type + '</span>\n' +
                            '                                <h3 class="card-title font-weight-normal text-truncate mb-3">\n' +
                            '                                    ' + label +  '</h3>\n' +
                            '                                <p class="card-text text-truncate">\n' +
                            '                                    ' + description + '\n' +
                            '                                </p>\n' +
                            '                            </div>\n' +
                            '                            <div class="card-footer d-flex flex-column justify-content-between p-0 text-sm-right">\n' +
                            '                                <a href="javascript:void(0)" class="display-5 font-weight-500 mb-6">\n' +
                            '                                    <i class="icon icon-calendar icon-fw mr-2"></i>\n' +
                            '                                    <span class="align-middle">' + formatStart + ' - ' + formatEnd + '</span>\n' +
                            '                                </a>\n' +
                            // '                                <a class="card-link font-weight-500" href="javascript:void(0)">\n' +
                            // '                                    <span>Check in detail</span> <i class="icon icon-arrow-long-right icon-lg ml-1"></i>\n' +
                            // '                                </a>\n' +
                            '                            </div>\n' +
                            '                        </div>\n' +
                            '                    </div>');
                    }

                    console.log(data);
                },
                error: function() {
                    console.log('error');
                }
            });

          _this.removeActiveClass()
          this.classList.add('cal__date--selected')
        })
      }
    },
  
    createMonth: function () {
      var currentMonth = this.date.getMonth()
      while (this.date.getMonth() === currentMonth) {
        this.createDay(this.date.getDate(), this.date.getDay(), this.date.getFullYear())
        this.date.setDate(this.date.getDate() + 1)
      }

      // while loop trips over and day is at 30/31, bring it back

        this.date.setDate(1)
      this.date.setMonth(this.date.getMonth() - 1)
  
      this.label.innerHTML = this.monthsAsString(this.date.getMonth()) + ' ' + this.date.getFullYear()
      this.dateClicked()
    },
  
    monthsAsString: function (monthIndex) {
      return [
        'Janvier',
        'Fevrier',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Aout',
        'Septembre',
        'Octobre',
        'Novembre',
        'Decembre'
      ][monthIndex]
    },
  
    clearCalendar: function () {
      vanillacalendar.month.innerHTML = ''
    },
  
    removeActiveClass: function () {
      for (var i = 0; i < this.activeDates.length; i++) {
        this.activeDates[i].classList.remove('cal__date--selected')
      }
    }
  }