<?php

namespace App\Controller\Back;

use App\Entity\Personne;
use App\Repository\PersonneRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user", name="user_")
 */
class PersonneController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(PersonneRepository $personneRepository): Response
    {
        return $this->render('back/user/index.html.twig', [
            'users' => $personneRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="show")
     */
    public function show(Personne $personne)
    {
        return $this->render('back/user/show.html.twig');
    }
}
