<?php

namespace App\Controller\Front;

use App\Entity\Structure;
use App\Repository\EnfantStructureRepository;
use App\Repository\StructureRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AnimatorController extends AbstractController
{
    /**
     * @Route("/animateur", name="animateur_show_listing", methods={"GET"})
     */
    public function index(StructureRepository $structureRepository): Response
    {
        $structures = $this->getUser()->getAnimatorstructures();

        return $this->render('animator/listing_structure_animateur.html.twig', [
            'structure' => $structures,
        ]);
    }

    /**
     * @Route("/animateur/{slug_structure}", name="animator_panel")
     */
    public function show(Structure $structure, EnfantStructureRepository $enfantStructureRepository): Response
    {

        $presenceTrue = $enfantStructureRepository->findBy([
            'structure' => $structure->getId(),
            'presence' => true,
            'validation' => 'Valide',
        ]);
        $presenceTrue = count($presenceTrue);

        $presenceFalse = $enfantStructureRepository->findBy([
            'structure' => $structure->getId(),
            'presence' => false,
            'validation' => 'Valide',
        ]);
        $presenceFalse = count($presenceFalse);

        $kids = $enfantStructureRepository->findBy([
            'structure' => $structure->getId(),
            'validation' => 'Valide',
        ]);

        $animators = $structure->getAnimatorsstructure();

        $getStructureCountKids = count($kids);

        return $this->render('animator/panel_animateur.html.twig', [
            'structure' => $structure,
            'animators' => $animators,
            'kids' => $kids,
            'getStructureCountKids' => $getStructureCountKids,
            'presenceTrue' => $presenceTrue,
            'presenceFalse' => $presenceFalse,
        ]);
    }

    /**
     * @Route("/{slug_structure}/pointage", name="animateur_show_pointage", methods={"GET"})
     */
    public function pointage(Structure $structure, EnfantStructureRepository $enfantStructureRepository): Response
    {
        $roleCurrentUser = $this->getUser()->getRoles();

        $idStructure = $structure->getId();

        $kids = $enfantStructureRepository->findBy([
            'structure' => $idStructure,
            'validation' => 'Valide',
        ]);

        $nameStructure = $structure->getSlugStructure();

        return $this->render('animator/outil_pointage.html.twig', [
            'structure' => $structure,
            'kids' => $kids,
            'nameStructure' => $nameStructure,
            'roleCurrentUser' => $roleCurrentUser,
        ]);
    }

    /**
     * @Route("/{slug_structure}/pointage/check", name="animateur_show_pointage_check", methods={"POST"})
     */
    public function pointageCheck(Structure $structure, EnfantStructureRepository $enfantStructureRepository, EntityManagerInterface $entityManager, Request $request)
    {
        $idStructure = $structure->getId();
        $idKid = $request->request->get('id');

        $valueBoolean = $request->request->get('boolean');
        $presence = $enfantStructureRepository->findOneBy([
            'structure' => $idStructure,
            'kid' => $idKid,
            'validation' => 'Valide',
        ]);
        $valueBoolean = filter_var($valueBoolean, FILTER_VALIDATE_BOOLEAN);

        $presence->setPresence($valueBoolean);

        $entityManager->persist($presence);
        $entityManager->flush();
        dump($presence);

        return new JsonResponse($valueBoolean);
    }

    /**
     * @Route("/{slug_structure}/pointage/reset", name="animateur_show_pointage_reset", methods={"POST"})
     */
    public function pointageReset(Structure $structure, EnfantStructureRepository $enfantStructureRepository, EntityManagerInterface $entityManager, Request $request)
    {
        $idStructure = $structure->getId();
        $valueBoolean = $request->request->get('boolean');
        $presence = $enfantStructureRepository->findBy([
            'structure' => $idStructure,
            'validation' => 'Valide',
        ]);
        $valueBoolean = filter_var($valueBoolean, FILTER_VALIDATE_BOOLEAN);

        foreach ($presence as $presences) {
            $presences->setPresence($valueBoolean);
            $entityManager->persist($presences);
            $entityManager->flush();
        }

        dump($presence);

        return new JsonResponse($valueBoolean);
    }
}
