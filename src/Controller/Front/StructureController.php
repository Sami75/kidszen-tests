<?php

namespace App\Controller\Front;

use App\Entity\Enfant;
use App\Entity\EnfantStructure;
use App\Entity\Structure;
use App\Form\EnfantCandidatureType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class StructureController extends AbstractController
{
    /**
     * @Route("/structure/", name="list_structure")
     */
    public function index()
    {
        $structures = $this->getDoctrine()->getRepository(Structure::class);

        return $this->render('default/structure/index.html.twig', [
            'structures' => $structures->findAll()
        ]);
    }

    /**
     * @Route("/structure/{slug_structure}", name="structure_show", methods={"GET", "POST"})
     */
    public function show(Structure $structure, Request $request)
    {

        if($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $enfant = $request->request->get('enfant');
            $getEntity = $this->getDoctrine()->getRepository(Enfant::class)->find($enfant);

            $enfantStructure = new EnfantStructure();

            $enfantStructure
                ->setKid($getEntity)
                ->setStructure($structure)
                ->setValidation('candidature')
                ->setApplicationCreatedAt(new \DateTime())
                ->setSlugEnfantStructure($getEntity->getSlug() . '-' . $structure->getSlugStructure())
                ;

            $em->persist($enfantStructure);
            $em->flush();

        }

        return $this->render('default/structure/show.html.twig', [
            'structure' => $structure
        ]);
    }
}
