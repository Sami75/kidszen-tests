<?php

namespace App\Controller\Front;

use App\Entity\Personne;
use App\Form\CompleteProfileType;
use App\Form\RegistrationType;
use App\Form\ResetPasswordType;
use App\Form\UserPasswordType;
use App\Repository\PersonneRepository;
use App\Service\ConnectedService;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Util\TargetPathTrait;


class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils, ConnectedService $connectedService)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $connectedService->checkConnected();
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        if ($error) {
            $this->addFlash(
                'error',
                $error->getMessageKey()
            );
        }

        return $this->render('default/auth/login.html.twig', ['last_username' => $lastUsername, 'error' => $error, 'login' => true]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }


    /**
     * @Route("/inscription", name="registration_family")
     */
    public function registration(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder,
                                 MailerService $mailerService, ConnectedService $connectedService)
    {

        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $connectedService->checkConnected();
        }

        $famille = new Personne();
        $form = $this->createForm(RegistrationType::class, $famille);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $famille->addRole('ROLE_NEW_FAMILLE');
            $token = $this->generateToken();
            $famille->setAccountconfirmationtoken($token);

            $manager->persist($famille);
            $manager->flush();
            $email = $famille->getEmail();
            // $mailerService->sendConfimationToken($email, 'KidsZen : Confirmation de compte ', $token, $famille, 'familyAccountConfirmation.html.twig');
            $this->addFlash('success', 'Votre inscription a été validée, Vous allez reçevoir un mail pour confirmer votre compte');
            return $this->redirectToRoute('front_login');
        }
        return $this->render('security/registration.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/account/confirm/{slug}/{token}", name="confirm_account")
     */
    public function confirmAccount(EntityManagerInterface $manager, PersonneRepository $personneRepository, $slug, $token,
                                   MailerService $mailerService, ConnectedService $connectedService)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $connectedService->checkConnected();
        }
        $famille = $personneRepository->findOneBy(['slug' => $slug]);
        $tokenExist = $famille->getAccountconfirmationtoken();
        if ($token === $tokenExist) {
            $famille->setAccountconfirmation(true);
            $manager->persist($famille);
            $manager->flush();
            $email = $famille->getEmail();
            $mailerService->sendConfimationToken($email, 'Bienvenue sur Kidszen ', null, $famille, 'familyWelcomeToKidszen.html.twig');
            $this->addFlash('success', 'Votre compte est validé, Vous pouvez vous connecter ');
            return $this->redirectToRoute('front_login');

//            return $this->redirectToRoute('front_registration_family');
        } else {
            return $this->render('security/resendAccountConfirmation.html.twig');
        }
    }

    /**
     * @Route("/account/resendConfirmation", name="resend_confirmation_token")
     */
    public function resendConfirmationToken(Request $request, EntityManagerInterface $manager, MailerService $mailerService,
                                            PersonneRepository $personneRepository, ConnectedService $connectedService)
    {

        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $connectedService->checkConnected();
        }

        if ($request->isMethod('post')) {
            $email = $request->request->get('email');
            $famille = $personneRepository->findOneBy(['email' => $email]);
            if (null === $famille) {
                $this->addFlash('not-user-exist', 'Si votre compte existe vous allez reçevoir un mail pour le confirmer sinon veuillez vous inscrire.');
                return $this->redirectToRoute('front_registration_family');
            }
            $token = $this->generateToken();
            $famille->setAccountconfirmationtoken($token);
            $manager->persist($famille);
            $manager->flush();

            $mailerService->sendConfimationToken($email, 'KidsZen : Confirmation de compte ', $token, $famille, 'familyAccountConfirmation.html.twig');
            $this->addFlash('success', 'Vous aller recevoir un email de confirmation pour activer votre compte et pouvoir vous connecter');

            return $this->redirectToRoute('front_login');
        }

        return $this->render('security/resendAccountConfirmation.html.twig', []);
    }


    /**
     * @Route("/account/forget_password", name="forget_password")
     */
    public function forgetPassword(Request $request, EntityManagerInterface $manager, MailerService $mailerService,
                                   PersonneRepository $personneRepository, ConnectedService $connectedService)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $connectedService->checkConnected();
        }

        if ($request->isMethod('post')) {
            $email = $request->request->get('email');
            $personne = $personneRepository->findOneBy(['email' => $email]);
            if (null === $personne) {
                $this->addFlash('not-user-exist', 'Si votre compte existe vous allez reçevoir un mail pour le réinitialiser sinon veuillez vous inscrire.');
                return $this->redirectToRoute('front_registration_family');
            }
            $token = $this->generateToken();
            $personne->setTokenresetpassword($token);
            $personne->setTokenresetpasswordcreatedtat(new \DateTime());
            $manager->persist($personne);
            $manager->flush();

            $mailerService->sendConfimationToken($email, "Kidszen : Réinitialisation mot de passe", $token, $personne, 'resetPasswordMail.html.twig');
            $this->addFlash('success', 'Vous avez reçu un mail pour réinitialiser votre mot de passe !');
            return $this->redirectToRoute('front_login');
        }

        return $this->render('security/forgetPassword.html.twig');

    }

    /**
     * @Route("/account/forget_password/{slug}/{token}", name="forget_password_confirmation")
     */
    public function resetPaswword(Request $request, EntityManagerInterface $manager, MailerService $mailerService, ConnectedService $connectedService,
                                  UserPasswordEncoderInterface $encoder, $slug, $token, PersonneRepository $personneRepository)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $connectedService->checkConnected();
        }
        $personne = $personneRepository->findOneBy(['slug' => $slug, 'tokenresetpassword' => $token]);

        $form = $this->createForm(ResetPasswordType::class, $personne);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if (null === $personne) {
                $this->addFlash('not_user_found', "Si votre compte existe vous allez reçevoir un mail pour réinitialiser le mot de passe sinon veuillez vous inscrire.");
                return $this->redirectToRoute('front_registration_family');
            }
            $personne->setPassword($encoder->encodePassword($personne, $form->get('plainPassword')->getData()));
            $manager->persist($personne);
            $manager->flush();
            $email = $personne->getEmail();
            $mailerService->sendConfimationToken($email, "Kidszen : Mot de passe est réinitialisé", $token, $personne, 'resetPasswordMail.html.twig');
            $this->addFlash('success', 'Votre mot de passe à été bien modifié.');
            return $this->redirectToRoute('front_login');
        }

        return $this->render('security/resetPassword.html.twig', [
            'form' => $form->createView(),
        ]);


    }


    /**
     * @return string
     * @throws \Exception
     */
    private function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }
}
