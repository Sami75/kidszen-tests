<?php

namespace App\Controller\Front;

use App\Entity\Actualite;
use App\Form\ActualiteType;
use App\Repository\ActualiteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/actualite")
 */
class ActualiteController extends AbstractController
{
    /**
     * @Route("/", name="actualite_index", methods={"GET"})
     */
    public function index(Request $request, ActualiteRepository $actualiteRepository): Response
    {
        //todo toast flash message 
        $actualite = new Actualite();

        $post = $request->query->get('post');

        $form = $this->createForm(ActualiteType::class, $actualite, ['action' => 'new']);

        return $this->render('/default/actualite/index.html.twig', [
            'actualites' => $actualiteRepository->findAll(),
            'form' => $form->createView(),
            'post' => $post
        ]);
    }

    /**
     * @Route("/new", name="actualite_new", methods={"GET","POST"})
     */
    public function new(Request $request, ActualiteRepository $actualiteRepository): Response
    {
        $actualite = new Actualite();
        $actualite->setAuthor($this->getUser());

        $form = $this->createForm(ActualiteType::class, $actualite, ['action' => 'new']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($actualite);
            $entityManager->flush();

            return $this->redirectToRoute('front_actualite_index', ['post' => $actualite->getId()]);
        }

        return $this->render('/default/actualite/index.html.twig', [
            'actualites' => $actualiteRepository->findAll(),
            'form' => $form->createView(),
            'post' => null
        ]);
    }

    // /**
    //  * @Route("/{id}", name="actualite_show", methods={"GET"})
    //  */
    // public function show(Actualite $actualite): Response
    // {
    //     return $this->render('actualite/show.html.twig', [
    //         'actualite' => $actualite,
    //     ]);
    // }

    /**
     * @Route("/{id}/edit", name="actualite_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Actualite $actualite, ActualiteRepository $actualiteRepository): Response
    {
        $form = $this->createForm(ActualiteType::class, $actualite, ['action' => 'edit']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('front_actualite_index', ['post' => $actualite->getId()]);
        }

        return $this->render('/default/actualite/index.html.twig', [
            'actualites' => $actualiteRepository->findAll(),
            'form' => $form->createView(),
            'post' => null
        ]);
    }

    /**
     * @Route("/{id}", name="actualite_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Actualite $actualite): Response
    {
        if ($this->isCsrfTokenValid('delete' . $actualite->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($actualite);
            $entityManager->flush();
        }

        return $this->redirectToRoute('front_actualite_index');
    }
}
