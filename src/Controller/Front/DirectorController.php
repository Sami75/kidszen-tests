<?php

namespace App\Controller\Front;

use App\Entity\Enfant;
use App\Entity\EnfantStructure;
use App\Repository\EnfantStructureRepository;
use App\Repository\StructureRepository;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

///**
// * @IsGranted("ROLE_ANIMATEUR")
// */
class DirectorController extends AbstractController
{
    /**
     * @Route("/directeur/", name="director_panel", methods={"GET"})
     */
    public function index(StructureRepository $structureRepository, EnfantStructureRepository $enfantStructureRepository): Response
    {
        $structure = $structureRepository->findOneBy(['director' => $this->getUser()]);
        $idStructure = $structure->getId();

        $presenceTrue = $enfantStructureRepository->findBy([
            'structure' => $idStructure,
            'presence' => true,
            'validation' => 'Valide',
        ]);
        $presenceTrue = count($presenceTrue);

        $presenceFalse = $enfantStructureRepository->findBy([
            'structure' => $idStructure,
            'presence' => false,
            'validation' => 'Valide',
        ]);
        $presenceFalse = count($presenceFalse);

        $animators = $structure->getAnimatorsstructure();

        $kids = $enfantStructureRepository->findBy([
            'structure' => $idStructure,
            'validation' => 'Valide',
        ]);

        $getStructureCountAnimator = $structure->getAnimatorsstructure()->count();
        $getStructureCountKids = count($kids);
        $getStructureCountAll = 1 + $getStructureCountAnimator + $getStructureCountKids;

        return $this->render('director/panel_directeur.html.twig', [
            'presenceTrue' => $presenceTrue,
            'presenceFalse' => $presenceFalse,
            'structure' => $structure,
            'animators' => $animators,
            'kids' => $kids,
            'getStructureCountAnimator' => $getStructureCountAnimator,
            'getStructureCountKids' => $getStructureCountKids,
            'getStructureCountAll' => $getStructureCountAll,
        ]);
    }

    /**
     * @Route("/directeur/candidature", name="director_candidature", methods={"GET"})
     */
    public function applicationList(EnfantStructureRepository $enfantStructureRepository, EntityManagerInterface $manager): Response
    {
        return $this->render('director/application.html.twig', [
            'applications' => $enfantStructureRepository->findBy([
                'structure' => $this->getUser()->getDirectorstructure(),
                'validation' => 'candidature',
            ]),
        ]);
    }

    /**
     * @Route("/directeur/candidature/history", name="director_candidature_history", methods={"GET"})
     */
    public function applicationHistory(EnfantStructureRepository $enfantStructureRepository): Response
    {
        return $this->render('director/application.html.twig', [
            'applications' => $enfantStructureRepository->findApplicationHistory($this->getUser()->getDirectorstructure()),
            'history' => true,
        ]);
    }

    /**
     * @Route("/directeur/candidature/{slugEnfantStructure}/accept", name="director_candidature_accept", methods={"GET","POST"})
     */
    public function applicationAccept(EnfantStructure $enfantStructure, EntityManagerInterface $manager,
                                      MailerService $mailerService): Response
    {
        $enfantStructure->setValidation('Valide')
            ->setResponseCreatedAt(new \DateTime());

        $manager->persist($enfantStructure);
        $manager->flush();
        $this->addFlash('success', 'Candidature accéptée. Une notification à "'.$enfantStructure->getKid()->getFamily()->getFirstname().
            ' '.$enfantStructure->getKid()->getFamily()->getFirstname().' a été envoyé');
        $mailerService->sendConfimationToken($enfantStructure->getKid()->getFamily()->getEmail(),
            'KidsZen : Candidature - Structure'.$enfantStructure->getStructure()->getLabel(),
            null, $enfantStructure, 'structureCandidature.html.twig');

        return $this->redirectToRoute('front_director_candidature');
    }

    /**
     * @Route("/directeur/candidature/{slugEnfantStructure}/refus", name="director_candidature_refus", methods={"GET","POST"})
     */
    public function applicationRefus(EnfantStructure $enfantStructure, EntityManagerInterface $manager,
                                     MailerService $mailerService): Response
    {
        $enfantStructure->setValidation('Refus')
            ->setResponseCreatedAt(new \DateTime());

        $manager->persist($enfantStructure);
        $manager->flush();
        $this->addFlash('success', 'Candidature réfusée. Une notification à "'.$enfantStructure->getKid()->getFamily()->getFirstname().
            ' '.$enfantStructure->getKid()->getFamily()->getFirstname().'" a été envoyé');
        $mailerService->sendConfimationToken($enfantStructure->getKid()->getFamily()->getEmail(),
            'KidsZen : Candidature - Structure'.$enfantStructure->getStructure()->getLabel(),
            null, $enfantStructure, 'structureCandidature.html.twig');

        return $this->redirectToRoute('front_director_candidature');
    }

    /**
     * @Route("/informations/enfant/{slug}", name="director_profil_enfant")
     */
    public function showKid(Enfant $enfant)
    {
        return $this->render('director/profil.enfant.html.twig', [
            'kid' => $enfant,
        ]);
    }

    /**
     * @Route("/directeur/enfants/", name="director_list_enfants")
     */
    public function showListKids(EnfantStructureRepository $enfantStructureRepository)
    {
        $director = $this->getUser();

        return $this->render('director/list.families.html.twig', [
            'EnfantStructures' => $enfantStructureRepository->findKidsStructure($director->getDirectorstructure()),
        ]);
    }

    /**
     * @Route("/directeur/enfant/{slugEnfantStructure}/active", name="director_enfants_active")
     */
    public function showActivAccountKid(EnfantStructure $enfantStructure, EntityManagerInterface $manager)
    {
        $value = $enfantStructure->getValidation();

        if ('Valide' === $value) {
            $enfantStructure->setValidation('Bloque');
        } elseif ('Bloque' === $value) {
            $enfantStructure->setValidation('Valide');
        }

        $manager->persist($enfantStructure);
        $manager->flush();

        $this->addFlash('success', 'Le compte de "'.$enfantStructure->getKid()->getFirstname().' '.
            $enfantStructure->getKid()->getLastname().'" a été modifié');

        return $this->redirectToRoute('front_director_list_enfants');
    }
}
