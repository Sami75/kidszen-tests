<?php

namespace App\Controller\Front;

use App\Entity\Personne;
use App\Form\CompleteProfileType;
use App\Form\ParametersUserAnimateurType;
use App\Form\ParametersUserCoordinateurType;
use App\Form\ParametersUserDirecteurType;
use App\Form\ParametersUserFamilleType;
use App\Repository\PersonneRepository;
use App\Service\ConnectedService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Runner\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user", name="user_")
 */
class PersonneController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(PersonneRepository $personneRepository): Response
    {
        return $this->render('front/user/profil.html.twig', [
            'users' => $personneRepository->findAll(),
        ]);
    }

    /**
     * @Route("/profil", name="profil_personne")
     */
    public function profil()
    {

        return $this->render('default/profil/profil.html.twig', [
            'controller_name' => 'ProfilPersonneController',
        ]);
    }

    /**
     * @Route("/parameters", name="parameters_personne")
     */
    public function parametres(Request $request)
    {
        switch ($this->getUser()->getRoles()[0]) {
            case 'ROLE_DIRECTEUR':
                $modifyInformations = $this->createForm(ParametersUserDirecteurType::class, $this->getUser());
                break;
            case 'ROLE_COORDINATEUR':
                $modifyInformations = $this->createForm(ParametersUserCoordinateurType::class, $this->getUser());
                break;
            case 'ROLE_ANIMATEUR':
                $modifyInformations = $this->createForm(ParametersUserAnimateurType::class, $this->getUser());
                break;
            case 'ROLE_FAMILLE':
                $modifyInformations = $this->createForm(ParametersUserFamilleType::class, $this->getUser());
                break;
            case 'ROLE_NEW_FAMILLE':
                return $this->redirectToRoute('front_user_complete_profil_family');
                break;

        }

        $modifyInformations->handleRequest($request);

        if ($modifyInformations->isSubmitted() && $modifyInformations->isValid()) {
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->render('default/profil/parameters.html.twig', [
            'form' => $modifyInformations->createView()
        ]);
    }

    /**
     * @Route("/complete_profil", name="complete_profil_family")
     */
    public function completeProfil(Request $request, EntityManagerInterface $manager, ConnectedService $connectedService)
    {

//        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
//            return $connectedService->checkConnected();
//        }

        $famille = $this->getUser();
        $form = $this->createForm(CompleteProfileType::class, $famille);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $famille->setRoles(["ROLE_FAMILLE"]);
            $manager->persist($famille);
            $manager->flush();

            $this->addFlash('success', 'Votre profil est complet. Désormais vous pouvez inscrire vos enfants dans nos structures, Veuillez vous reconnectez ');
            return $this->redirectToRoute('front_user_profil_personne');
        }
        return $this->render('security/completeProfil.html.twig', [
            'form' => $form->createView(),
        ]);

    }
}
