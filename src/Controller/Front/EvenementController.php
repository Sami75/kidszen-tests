<?php

namespace App\Controller\Front;

use App\Entity\Evenement;
use App\Form\EvenementType;
use App\Repository\EvenementRepository as RepositoryEvent;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class EvenementController extends AbstractController
{
    /**
     * @Route("/events", name="events_calendar", methods={"POST", "GET"})
     */
    public function index(Request $request, RepositoryEvent $repositoryEvent)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $evenement = new Evenement();
        $evenement->setCreator($user);

        $form = $this->createForm(EvenementType::class, $evenement);

        if ($request->isXmlHttpRequest()) {
            $start = $request->request->get('start');
            $end = $request->request->get('end');

            $roles = $user->getRoles();

            if (in_array('ROLE_ADMIN', $roles) || in_array('ROLE_DIRECTEUR', $roles) || in_array('ROLE_COORDINATEUR', $roles)) {
                $events = $em
                ->getRepository(Evenement::class)
                ->getAllEvents($start, $end);
            } elseif (in_array('ROLE_ANIMATEUR', $roles)) {
                $events = $em
                ->getRepository(Evenement::class)
                ->getUserEvents($start, $end, $user->getId());
            } elseif (in_array('ROLE_FAMILLE', $roles)) {
                $kids = $user->getKids();
                $events = new ArrayCollection();

                foreach ($kids as $kid) {
                    $events = $em
                    ->getRepository(Evenement::class)
                    ->getKidEvent($start, $end, $kid->getId());
                }
            }

            $serializer = new Serializer([new ObjectNormalizer()]);
            $data = [];

            foreach ($events as $event) {
                $data[] = $serializer->normalize($event, null, [AbstractNormalizer::ATTRIBUTES => ['id', 'label', 'description', 'startdate', 'enddate', 'type', 'kidsevent' => ['id', 'firstname', 'lastname', 'family' => ['id']], 'creator' => ['id', 'firstname', 'lastname'], 'eventresponsible' => ['id', 'firstname', 'lastname']]]);
            }

            return new JsonResponse($data);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($evenement);
            $entityManager->flush();

            return $this->redirectToRoute('front_events_calendar');
        }

        return $this->render('default/events/index.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/events/{id}", name="events_calendar_detail", methods={"POST", "GET"})
     */
    public function edit(Request $request, Evenement $evenement)
    {
        $form = $this->createForm(EvenementType::class, $evenement);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('front_events_calendar_detail', ['id' => $evenement->getId()]);
        }

        return $this->render('default/events/detail.html.twig', ['evenement' => $evenement, 'form' => $form->createView()]);
    }

    /**
     * @Route("/{id}", name="events_calendar_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Evenement $evenement): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evenement->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($evenement);
            $entityManager->flush();
        }

        return $this->redirectToRoute('front_events_calendar');
    }
}
