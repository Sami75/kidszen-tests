<?php

namespace App\Controller\Front;

use App\Entity\Enfant;
use App\Form\EnfantType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EnfantController extends AbstractController
{

    /**
     * @Route("/enfant/", name="index_enfant")
     */
    public function index(Request $request)
    {
        return $this->render('default/enfant/index.html.twig');
    }

    /**
     * @Route("/enfant/add", name="enfant_add")
     */
    public function add(Request $request)
    {
        $enfant = new Enfant();
        $form   = $this->createForm(EnfantType::class, $enfant);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($enfant);
            $entityManager->flush();

            return $this->redirectToRoute('front_profil_enfant', ['slug' => $enfant->getSlug()]);
        }

        return $this->render('default/enfant/add.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/enfant/{slug}", name="profil_enfant")
     */
    public function show(Enfant $enfant, Request $request)
    {
        $form   = $this->createForm(EnfantType::class, $enfant);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($enfant);
            $entityManager->flush();

            return $this->redirectToRoute('front_profil_enfant', ['slug' => $enfant->getSlug()]);
        }

        return $this->render('default/enfant/profil.html.twig', [
            'kid' => $enfant,
            'form' => $form->createView()
        ]);
    }
}
