<?php

namespace App\Controller\Front;

use App\Entity\Structure;
use App\Repository\EnfantStructureRepository;
use App\Repository\StructureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CoordinatorController extends AbstractController
{
    /**
     * @Route("/coordinateur", name="coordinator_show_listing", methods={"GET"})
     */
    public function index(StructureRepository $structureRepository): Response
    {
        return $this->render('coordinator/listing_structure.html.twig', [
            'structure' => $structureRepository->findAll(),
        ]);
    }

    /**
     * @Route("/coordinateur/{slug_structure}", name="coordinator_show_information", methods={"GET"})
     */
    public function show(Structure $structure, EnfantStructureRepository $enfantStructureRepository): Response
    {
        $kids = $enfantStructureRepository->findBy([
            'structure' => $structure->getId(),
            'validation' => 'Valide',
        ]);

        $getStructureCountAnimator = $structure->getAnimatorsstructure()->count();
        $getStructureCountKids = count($kids);
        $animators = $structure->getAnimatorsstructure();

        return $this->render('coordinator/informations_structure.html.twig', [
            'getStructureCountAnimator' => $getStructureCountAnimator,
            'getStructureCountKids' => $getStructureCountKids,
            'animators' => $animators,
            'structure' => $structure,
        ]);
    }
}