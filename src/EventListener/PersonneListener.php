<?php


namespace App\EventListener;


use App\Entity\Personne;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PersonneListener
{

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function prePersist( Personne $personne, LifecycleEventArgs $eventArgs){

        if($personne->getPlainPassword()){
            $personne->setPassword($this->passwordEncoder->encodePassword($personne,$personne->getPlainPassword()));
        }

    }

}
