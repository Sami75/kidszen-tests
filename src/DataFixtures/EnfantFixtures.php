<?php

namespace App\DataFixtures;

use App\Entity\Enfant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class EnfantFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 15; $i++) {
            $enfant = new Enfant();

            $randomGender = ['female', 'male'];

            $medical    = ['Intolérent au lait', 'Diabétique', 'Urticaire', 'Eczéma', 'Acariens'];
            $diet       = ['Sans porc', 'Sans arachide'];

            $enfant
                ->setAddress($faker->address)
                ->setBirthday($faker->dateTimeBetween('-10 years', '-2 years'))
                ->setCity($faker->city)
                ->setFamily($this->getReference('famille' . $faker->numberBetween(0, 1)))
                ->setLastname($faker->lastName)
                ->setFirstname($faker->firstName)
                ->setGender($randomGender[rand(0, 1)])
                ->setPostalcode($faker->postcode)
                ->setCity($faker->city)
                ->setDiet($diet[rand(0, count($diet) - 1)])
                ->setMidicalinfo($medical[rand(0, count($medical) - 1)])
                ;

            $this->setReference('enfant' . $i, $enfant);

            $manager->persist($enfant);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            PersonneFixtures::class,
        );
    }
}
