<?php

namespace App\DataFixtures;

use App\Entity\EnfantStructure;
use App\Entity\Structure;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class EnfantStructureFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();

        $faker = \Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 15; $i++) {
            $enfantStructure = new EnfantStructure();

            $enfantStructure
                ->setKid($this->getReference('enfant'.$i))
                ->setStructure($this->getReference('structure'.$faker->numberBetween(0, 1)))
                ->setValidation('candidature')
                ->setPresence(false)
                ->setApplicationCreatedAt(new \DateTime())
                ->setSlugEnfantStructure($enfantStructure->createSlug())
                ;

            $manager->persist($enfantStructure);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            StructureFixtures::class,
        );
    }
}
