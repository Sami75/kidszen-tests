<?php

namespace App\DataFixtures;

use App\Repository\PersonneRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class EmploiTempsEvenementFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        for ($i = 0; $i < 10; $i++) {
            $emploi = $this->getReference('emploi' . $i);

            for ($a = 0; $a < 10; $a++) {
                $evenement = $this->getReference('evenement' . $i);

                $emploi->addEvenement($evenement);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            EmploiTempsFixtures::class,
        );
    }
}
