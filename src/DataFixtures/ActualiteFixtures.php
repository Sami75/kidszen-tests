<?php

namespace App\DataFixtures;

use App\Entity\Actualite;
use App\Entity\Personne;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ActualiteFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $faker = \Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 10; $i++) {
            $actualite = new Actualite();

            $actualite
                ->setLabel('Lorem ipsum article n°' . $i)
                ->setDescription($faker->realText(200))
                ->setAuthor($this->getReference('animateur'.$faker->numberBetween(0, 2)))
                ;

//            dump($this->getReference('personne' . $i));

            $manager->persist($actualite);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            PersonneFixtures::class,
        );
    }
}
