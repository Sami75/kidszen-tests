<?php

namespace App\DataFixtures;

use App\Entity\Evenement;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class EvenementFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker  = \Faker\Factory::create('fr_FR');
        $type   = ['Réunion', 'Brainstorming', 'Jeu de société', 'Sortie Scolaire', 'Cinéma'];

        $countKidsEvent = 0;

        for ($i = 0; $i < 10; $i++) {
            $random     = rand(0, count($type) - 1);
            $randomType = $type[$random];

            $evenement = new Evenement();

            switch ($random) {
                case 0:
                case 1:
                    $evenement->setCreator($this->getReference('directeur' . $faker->numberBetween(0, 1)));
                    $evenement->setLabel('Réunion & Brainstorming');
                    break;
                default:
                    $evenement->setCreator($this->getReference('animateur' . $faker->numberBetween(0, 1)));
                    $evenement->setLabel('Activité : ' . $randomType);
                    $this->setReference('eventKids' . $countKidsEvent, $evenement);
                    $countKidsEvent++;
                    break;
            }

            $evenement
                ->setDescription($faker->realText())
                ->setType($randomType)
                ->setMbmaxchildren($faker->numberBetween(8, 23))
                ->setStartdate($faker->dateTimeBetween('-5 hours', '-2 hours'))
                ->setEnddate($faker->dateTimeBetween('-2 hours', '-1 hours'))
                ;

            $this->setReference('evenement' . $i, $evenement);

            $manager->persist($evenement);
        }

        $manager->flush();
    }
}
