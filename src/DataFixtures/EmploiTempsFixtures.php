<?php

namespace App\DataFixtures;

use App\Entity\EmploiTemps;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class EmploiTempsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();

        $faker = \Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 15; $i++) {
            $emploi = new EmploiTemps();

            $emploi
                ->setStartdate($faker->dateTimeBetween('+1 month', '+4 months'))
                ->setEnddate($faker->dateTimeBetween('+4 months', '+8 months'))
                ;

            $this->addReference('emploi' . $i, $emploi);

            $manager->persist($emploi);
        }

        $manager->flush();
    }
}
