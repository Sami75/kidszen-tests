<?php

namespace App\DataFixtures;

use App\Entity\Structure;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class StructureFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 2; ++$i) {
            $structure = new Structure();

            $structure
                ->setLabel('Structure '.$faker->company)
                ->setAddress($faker->address)
                ->setCity($faker->city)
                ->setCoordinator($this->getReference('coordinateur'.$i))
                ->setCountry($faker->country)
                ->setDirector($this->getReference('directeur'.$i))
                ->setEmail($faker->email)
                ->setPostalcode($faker->postcode)
                ->setTel($faker->phoneNumber)
                ->setBudget(rand(1000, 100000000))
                ;

            $this->setReference('structure'.$i, $structure);

            $manager->persist($structure);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            PersonneFixtures::class,
        ];
    }
}
