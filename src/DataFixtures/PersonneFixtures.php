<?php

namespace App\DataFixtures;

use App\Entity\Personne;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PersonneFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $incFamille = 0;
        $incCoord = 0;
        $incAnimateur = 0;
        $incDirecteur = 0;

        for ($i = 0; $i < 20; ++$i) {
            $faker = \Faker\Factory::create('fr_FR');

            $personne = new Personne();

//            ROLE_ADMIN, ROLE_FAMILLE, ROLE_COORDINATEUR, ROLE_ANIMATEUR, ROLE_DIRECTEUR

            $personne
                ->setLastname($faker->lastName)
                ->setFirstname($faker->firstName)
                ->setBirthday($faker->dateTimeBetween('-50 years', '-25 years'))
                ->setTel($faker->phoneNumber)
                ->setEmail($faker->safeEmail)
                ->setPassword($this->passwordEncoder->encodePassword($personne, '123AZEaze'))
                ->setGender($faker->randomElement(['a', 'f', 'm']))
                ->setAddress($faker->streetAddress)
                ->setPostalcode($faker->postcode)
                ->setCity($faker->city)
                ->setAccountconfirmation(true);

            switch ($i) {
                case 0:
                    $role = 'ROLE_ADMIN';
                    $this->setReference('admin'.$i, $personne);
                    break;
                case 1:
                case 2:
                    $role = 'ROLE_DIRECTEUR';
                    $this->setReference('directeur'.$incDirecteur, $personne);
                    ++$incDirecteur;
                    break;
                case 3:
                case 4:
                case 5:
                    $role = 'ROLE_COORDINATEUR';
                    $this->setReference('coordinateur'.$incCoord, $personne);
                    ++$incCoord;
                    break;
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                    $role = 'ROLE_ANIMATEUR';
                    $this->setReference('animateur'.$incAnimateur, $personne);
                    ++$incAnimateur;
                    break;
                default:
                    $role = 'ROLE_FAMILLE';
                    $this->setReference('famille'.$incFamille, $personne);
                    ++$incFamille;
            }

            $personne->addRole($role);

            $manager->persist($personne);
        }

        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
