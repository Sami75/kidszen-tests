<?php

namespace App\DataFixtures;

use App\Entity\Evenement;
use App\Entity\Personne;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class EvenementPersonneFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $evenement  = $this->getReference('evenement' . $i);
            $type       = $evenement->getType();

            switch ($type) {
                case 'Brainstorming':
                case 'Réunion':
                    $personne = $this->getReference('directeur' . rand(0, 1));
                    break;
                default:
                    $personne = $this->getReference('animateur' . rand(0, 2));
            }

            $personne->addEvent($evenement);

            $manager->persist($personne);
        }

        $manager->flush();
    }
}
