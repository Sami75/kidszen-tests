<?php

namespace App\DataFixtures;

use App\Entity\Evenement;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;

class EvenementEnfantFixtures extends Fixture implements DependentFixtureInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function load(ObjectManager $manager)
    {
        $em = $this->em;

        $kidsEvent = $em->getRepository(Evenement::class)->getKidsEvent();

        for ($i = 0; $i < count($kidsEvent); $i++) {
            $evenement = $this->getReference('eventKids' . $i);
            $nbEnfant  = rand(1, 4);

            for ($a = 0; $a < $nbEnfant; $a++) {
                $enfant = $this->getReference('enfant' . $a);

                $enfant->addEvent($evenement);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            EvenementFixtures::class,
        );
    }
}
