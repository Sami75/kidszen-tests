<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class StructurePersonneFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $structure = $this->getReference('structure' . rand(0, 1));
            $animateur = $this->getReference('animateur' . $i);

            $structure->addAnimatorsstructure($animateur);

            $manager->persist($structure);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
          StructureFixtures::class
        );
    }
}
