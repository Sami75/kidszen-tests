<?php

namespace App\Repository;

use App\Entity\EnfantStructure;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method EnfantStructure|null find($id, $lockMode = null, $lockVersion = null)
 * @method EnfantStructure|null findOneBy(array $criteria, array $orderBy = null)
 * @method EnfantStructure[]    findAll()
 * @method EnfantStructure[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EnfantStructureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EnfantStructure::class);
    }

    /**
    * @return EnfantStructure[] Returns an array of EnfantStructure objects
    */
    public function findApplicationHistory($structure)
    {
        return $this->createQueryBuilder('e')
            ->andWhere("e.validation LIKE 'Valide' ")
            ->andWhere("e.structure = :structure ")
            ->setParameter(':structure', $structure)
            ->orWhere("e.validation LIKE 'Refus'")
            ->orderBy('e.ResponseCreatedAt', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return EnfantStructure[] Returns an array of EnfantStructure objects
     */
    public function findKidsStructure($structure)
    {
        return $this->createQueryBuilder('e')
            ->andWhere("e.structure = :structure ")
            ->setParameter(':structure', $structure)
            ->andWhere("e.validation LIKE 'Valide' ")
            ->orWhere("e.validation LIKE 'Bloque'")
            ->orderBy('e.ResponseCreatedAt', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    /*
    public function findOneBySomeField($value): ?EnfantStructure
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
