<?php

namespace App\Repository;

use App\Entity\Evenement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Evenement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Evenement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Evenement[]    findAll()
 * @method Evenement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvenementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Evenement::class);
    }

    // /**
    //  * @return Evenement[] Returns an array of Evenement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function getKidsEvent()
    {
        return $this->createQueryBuilder('t')
            ->select('t.id')
            ->where('t.type != :type1')
            ->andWhere('t.type != :type2')
            ->setParameters(['type1' => 'Réunion', 'type2' => 'Brainstorming'])
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return mixed
     */
    public function getReunionEvents()
    {
        return $this->createQueryBuilder('t')
            ->select('t.id')
            ->where('t.type = :type1')
            ->orWhere('t.type = :type2')
            ->setParameters(['type1' => 'Réunion', 'type2' => 'Brainstorming'])
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function findOneBySomeField($value)
    {
        return $this->createQueryBuilder('e')
            ->select('e.label', 'e.description', 'e.startdate', 'e.enddate', 'e.type')
            ->where(':val BETWEEN e.startdate AND e.enddate')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param $start
     * @param $end
     *
     * @return mixed
     */
    public function getAllEvents($start, $end)
    {
        return $this->createQueryBuilder('e')
            // ->select('e.id, e.label', 'e.description', 'e.startdate', 'e.enddate', 'e.type')
            ->where(' e.startdate BETWEEN :start AND :end')
            ->orWhere('e.enddate BETWEEN :start AND :end')
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getUserEvents($start, $end, $id)
    {
        $entityManager = $this->getEntityManager();

        // dd($start, $end); 17/05 20/05
        $query = $entityManager->createQuery(
            'SELECT e, a
            FROM App\Entity\Evenement e
            INNER JOIN e.eventresponsible a
            WHERE a.id = :id
            AND :start <= e.startdate
            AND :end >= e.enddate'
        )->setParameter('id', $id)
        ->setParameter('start', new \Datetime($start))
        ->setParameter('end', new \Datetime($end));

        return $query->getResult();
    }

    public function getKidEvent($start, $end, $id)
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT e, k
            FROM App\Entity\Evenement e
            INNER JOIN e.kidsevent k
            WHERE k.id = :id
            AND :start >= e.startdate
            AND :end <= e.enddate
            OR  e.startdate BETWEEN :start AND :end
            AND e.enddate BETWEEN :start AND :end
            OR :start >= e.startdate
            OR :end <= e.enddate'
        )->setParameter('id', $id)
        ->setParameter('start', new \Datetime($start))
        ->setParameter('end', new \Datetime($end));

        return $query->getResult();
    }
}
