<?php

namespace App\Service;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;

class MailerService
{
    /**
     * @var MailerInterface
     */
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendConfimationToken($to, $object, $token = null, $entity, $view)
    {
        $message = (new TemplatedEmail())
            ->from('kkidszen@gmail.com')
            ->to('kkidszen@gmail.com') //$to
            ->subject($object)
            ->text('ça va kidszn !')
            ->htmlTemplate('emails/'.$view)
            ->context([
                'expiration_date' => new \DateTime('+1 days'),
                'user' => $entity,
                'token' => $token,
                ])
                ;
        $sentTokenEmail = $this->mailer->send($message);

        return true;
    }
}
