<?php


namespace App\Service;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class ConnectedService
{

    private $security;
    private $urlGenerator;
    public function __construct(Security $security,UrlGeneratorInterface $urlGenerator)
    {
        $this->security = $security;
        $this->urlGenerator =$urlGenerator;
    }

    public function checkConnected()
    {
        if($this->security->isGranted('IS_AUTHENTICATED_FULLY')){
            $user = $this->security->getUser();
            $redirection="";
            switch ($user->getRoles()[0]){
                case 'ROLE_FAMILLE': $redirection="front_default_index"; break;
                case 'ROLE_DIRECTEUR': $redirection="front_director_panel"; break;
                case 'ROLE_COORDINATEUR': $redirection="front_coordinator_show_listing"; break;
                case 'ROLE_ANIMATEUR': $redirection="front_animateur_show_listing"; break;
                case 'ROLE_ADMIN': $redirection="front_resend_confirmation_token"; break;
            }

            return new RedirectResponse($this->urlGenerator->generate("$redirection"));

        }
    }
}
