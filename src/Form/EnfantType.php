<?php

namespace App\Form;

use App\Entity\Enfant;
use App\Form\Type\BirthdayCustomType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Contracts\Translation\TranslatorInterface;

class EnfantType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'label' => $this->translator->trans('kidPage.formAdd.labels.firstname'),
                'attr' => [
                    'placeholder' => $this->translator->trans('kidPage.formAdd.labels.firstname')
                ]
            ])
            ->add('lastname',TextType::class, [
                'label' => $this->translator->trans('kidPage.formAdd.labels.lastname'),
                'attr' => [
                    'placeholder' => $this->translator->trans('kidPage.formAdd.labels.lastname')
                ]
            ])
            ->add('birthday', BirthdayCustomType::class, [
                'label' => $this->translator->trans('kidPage.formAdd.labels.birthday'),
                'attr' => [
                    'placeholder' => $this->translator->trans('kidPage.formAdd.labels.birthday')
                ],
                'widget' => 'single_text'
            ])
            ->add('gender', ChoiceType::class, [
                'label' => $this->translator->trans('kidPage.formAdd.labels.gender'),
                'choices'  => [
                    $this->translator->trans('kidPage.formAdd.desc.genderKid') => null,
                    $this->translator->trans('kidPage.formAdd.desc.gender.male') => 'male',
                    $this->translator->trans('kidPage.formAdd.desc.gender.female') => 'female',
                ],
            ])
            ->add('address', TextType::class, [
                'label' => $this->translator->trans('kidPage.formAdd.labels.address'),
                'attr' => [
                    'placeholder' => $this->translator->trans('kidPage.formAdd.labels.address')
                ]
            ])
            ->add('postalcode', TextType::class, [
                'label' => $this->translator->trans('kidPage.formAdd.labels.postalcode'),
                'attr' => [
                    'placeholder' => $this->translator->trans('kidPage.formAdd.labels.postalcode')
                ]
            ])
            ->add('city', TextType::class, [
                'label' => $this->translator->trans('kidPage.formAdd.labels.city'),
                'attr' => [
                    'placeholder' => $this->translator->trans('kidPage.formAdd.labels.city')
                ]
            ])
            ->add('photo', FileType::class, [
                'label' => $this->translator->trans('kidPage.formAdd.labels.photo'),
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '2048k',
                        'mimeTypes' => [
                            'image/png'
                        ],
                        'mimeTypesMessage' => $this->translator->trans('kidPage.formAdd.desc.badFile'),
                    ])
                ],
                'attr' => [
                    'placeholder' => $this->translator->trans('kidPage.formAdd.desc.photo')
                ]
            ])
            ->add('diet', TextType::class, [
                'label' => $this->translator->trans('kidPage.formAdd.labels.diet'),
                'attr' => [
                    'placeholder' => $this->translator->trans('kidPage.formAdd.desc.diet')
                ]
            ])
            ->add('numsocialsecurity', TextType::class, [
                'label' => $this->translator->trans('kidPage.formAdd.labels.numsocialsecurity'),
                'attr' => [
                    'placeholder' => $this->translator->trans('kidPage.formAdd.labels.numsocialsecurity')
                ]
            ])
            ->add('numpublicliability', TextType::class, [
                'label' => $this->translator->trans('kidPage.formAdd.labels.numpublicliability'),
                'attr' => [
                    'placeholder' => $this->translator->trans('kidPage.formAdd.labels.numpublicliability')
                ]
            ])
            ->add('namepublicliability', TextType::class, [
                'label' => $this->translator->trans('kidPage.formAdd.labels.namepublicliability'),
                'attr' => [
                    'placeholder' => $this->translator->trans('kidPage.formAdd.labels.namepublicliability')
                ]
            ])
            ->add('midicalinfo', TextareaType::class, [
                'label' => $this->translator->trans('kidPage.formAdd.labels.midicalinfo'),
                'attr' => [
                    'placeholder' => $this->translator->trans('kidPage.formAdd.labels.midicalinfo')
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Enfant::class,
        ]);
    }
}
