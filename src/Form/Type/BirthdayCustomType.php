<?php


namespace App\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BirthdayCustomType extends AbstractType
{

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'input_options'  => [],
            'error_bubbling' => false,
        ]);
    }

//    public function getBlockPrefix()
//    {
//        return 'app_';
//    }

    public function getParent()
    {
        return BirthdayType::class;
    }

}
