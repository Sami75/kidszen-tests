<?php

namespace App\Form;

use App\Entity\Enfant;
use App\Entity\Evenement;
use App\Entity\Personne;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class EvenementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAction($options['action'])
            ->setMethod('POST')
            // ->add('id', HiddenType::class)
            ->add('label')
            ->add('description')
            ->add('type', ChoiceType::class, [
                'choices' => ['Réunion' => 'reunion', 'Cinéma' => 'cinema', 'Jeu de société' => 'jeu_societe', 'Sortie' => 'sortie'],
            ])
            ->add('startdate', DateTimeType::class, [
                'widget' => 'single_text',
                'label' => 'Date de début',
            ])
            ->add('enddate', DateTimeType::class, [
                'widget' => 'single_text',
                'label' => 'Date de fin',
            ])
            ->add('kidsevent', EntityType::class, [
                'class' => Enfant::class,
                'choice_label' => 'firstname',
                'label' => 'Ajouter des enfants à l\'évenement',
                'multiple' => true,
                ])
            ->add('eventresponsible', EntityType::class, [
                'class' => Personne::class,
                'choice_label' => 'firstname',
                'label' => 'Ajouter des responsable de l\'évenement',
                'multiple' => true,
            ])
            ->add('update', HiddenType::class, [
                'data' => false,
                'label' => "update",
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Evenement::class,
        ]);
    }
}
