<?php

namespace App\Form;

use App\Entity\Personne;
use App\Form\Type\BirthdayCustomType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParametersUserCoordinateurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname')
            ->add('firstname')
            ->add('birthday', BirthdayCustomType::class, [
                'widget' => 'single_text'
            ])
            ->add('tel')
            ->add('email')
            ->add('gender')
            ->add('address')
            ->add('postalcode')
            ->add('city')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Personne::class,
        ]);
    }
}
