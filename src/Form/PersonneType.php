<?php

namespace App\Form;

use App\Entity\Personne;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('roles')
            ->add('lastname')
            ->add('firstname')
            ->add('birthday')
            ->add('tel')
            ->add('email')
            ->add('password')
            ->add('accountconfirmation')
            ->add('accountconfirmationtoken')
            ->add('gender')
            ->add('address')
            ->add('postalcode')
            ->add('city')
            ->add('photo')
            ->add('diploma')
            ->add('certification')
            ->add('townhall')
            ->add('townhallpostalcode')
            ->add('townhallcity')
            ->add('townhalldepartment')
            ->add('townhalltel')
            ->add('townhallemail')
            ->add('activation')
            ->add('situation')
            ->add('slug')
            ->add('events')
            ->add('directorstructure')
            ->add('animatorstructures')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Personne::class,
        ]);
    }
}
