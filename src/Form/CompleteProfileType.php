<?php

namespace App\Form;

use App\Entity\Personne;
use App\Form\Type\BirthdayCustomType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompleteProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname', TextType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'Prénom'],
                'disabled' => true
            ])
            ->add('firstname', TextType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'Nom'],
                'disabled' => true
            ])
            ->add('birthday', BirthdayCustomType::class,[
                'label' => 'date de naissance',
                'widget' => 'single_text',

            ])
            ->add('tel', TelType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'Téléphone']
            ])
            ->add('email', EmailType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'Email'],
                'disabled' => true
            ])
            ->add('gender', ChoiceType::class, [
                'label' => false,
                'choices' => Personne::GENDER
            ])
            ->add('address', TextType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'Adresse']
            ])
            ->add('postalcode', TextType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'Code postal']
            ])
            ->add('city', TextType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'Adresse']
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Personne::class,
            'validation_groups' => ['complete'],

        ]);
    }
}
