<?php

namespace App\Form;

use App\Entity\Personne;
use App\Form\Type\BirthdayCustomType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname', TextType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'Prénom']
            ])
            ->add('firstname', TextType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'Nom']
            ])
//            ->add('birthday', BirthdayCustomType::class,[
//                'label' => 'date de naissance',
//                'widget' => 'single_text',
//
//            ])
//            ->add('tel', TelType::class, [
//                'label' => false,
//                'attr' => ['placeholder' => 'Téléphone']
//            ])
            ->add('email', EmailType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'Email']
            ]);
//            ->add('gender', ChoiceType::class, [
//                'label' => false,
//                'choices' => Personne::GENDER
//            ])
//            ->add('address', TextType::class, [
//                'label' => false,
//                'attr' => ['placeholder' => 'Adresse']
//            ])
//            ->add('postalcode', TextType::class, [
//                'label' => false,
//                'attr' => ['placeholder' => 'Code postal']
//            ])
//            ->add('city', TextType::class, [
//                'label' => false,
//                'attr' => ['placeholder' => 'Adresse']
//            ]);

        if ($options['plainPassword']) {
            $builder->add('plainPassword',RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Veuillez mettre le même mot de passe',
                'first_options' => ['label' =>false,'attr' => ['placeholder' => 'mot de passe'] ],
                'second_options' =>['label' =>false,'attr' => ['placeholder' => 'confirmation mot de passe'] ],
            ]);

        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Personne::class,
            'validation_groups' => ['Default','resetPassword'],
            'plainPassword' => true,
        ]);
    }
}
