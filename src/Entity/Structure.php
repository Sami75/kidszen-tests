<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StructureRepository")
 * @ORM\Table(schema="kz")
 */
class Structure
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postalcode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $budget;

    /**
     * @return mixed
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * @param mixed $budget
     */
    public function setBudget($budget): void
    {
        $this->budget = $budget;
    }


    /**
     * @ORM\Column(type="string", unique=true)
     * @Gedmo\Slug(fields={"label"})
     */
    private $slug_structure;

    /**
     * @return mixed
     */
    public function getSlugStructure()
    {
        return $this->slug_structure;
    }

    /**
     * @param mixed $slug_structure
     */
    public function setSlugStructure($slug_structure): void
    {
        $this->slug_structure = $slug_structure;
    }


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Personne", inversedBy="coordinatorstructures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $coordinator;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Personne", inversedBy="directorstructure", cascade={"persist", "remove"})
     */
    private $director;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Personne", inversedBy="animatorstructures")
     * @ORM\JoinTable(schema="kz")
     */
    private $animatorsstructure;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EnfantStructure", mappedBy="structure", orphanRemoval=true)
     */
    private $enfantStructures;

    public function __construct()
    {
        $this->animatorsstructure = new ArrayCollection();
        $this->enfantStructures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPostalcode(): ?string
    {
        return $this->postalcode;
    }

    public function setPostalcode(string $postalcode): self
    {
        $this->postalcode = $postalcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getCoordinator(): ?Personne
    {
        return $this->coordinator;
    }

    public function setCoordinator(?Personne $coordinator): self
    {
        $this->coordinator = $coordinator;

        return $this;
    }

    public function getDirector(): ?Personne
    {
        return $this->director;
    }

    public function setDirector(?Personne $director): self
    {
        $this->director = $director;

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getAnimatorsstructure(): Collection
    {
        return $this->animatorsstructure;
    }

    public function addAnimatorsstructure(Personne $animatorsstructure): self
    {
        if (!$this->animatorsstructure->contains($animatorsstructure)) {
            $this->animatorsstructure[] = $animatorsstructure;
        }

        return $this;
    }

    public function removeAnimatorsstructure(Personne $animatorsstructure): self
    {
        if ($this->animatorsstructure->contains($animatorsstructure)) {
            $this->animatorsstructure->removeElement($animatorsstructure);
        }

        return $this;
    }

    /**
     * @return Collection|EnfantStructure[]
     */
    public function getEnfantStructures(): Collection
    {
        return $this->enfantStructures;
    }

    public function addEnfantStructure(EnfantStructure $enfantStructure): self
    {
        if (!$this->enfantStructures->contains($enfantStructure)) {
            $this->enfantStructures[] = $enfantStructure;
            $enfantStructure->setStructure($this);
        }

        return $this;
    }

    public function removeEnfantStructure(EnfantStructure $enfantStructure): self
    {
        if ($this->enfantStructures->contains($enfantStructure)) {
            $this->enfantStructures->removeElement($enfantStructure);
            // set the owning side to null (unless already changed)
            if ($enfantStructure->getStructure() === $this) {
                $enfantStructure->setStructure(null);
            }
        }

        return $this;
    }
}
