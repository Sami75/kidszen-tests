<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @ORM\Entity(repositoryClass="App\Repository\EnfantStructureRepository")
 * @ORM\Table(schema="kz")
 */
class EnfantStructure
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="App\Entity\Enfant", inversedBy="enfantStructures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $kid;


    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="App\Entity\Structure", inversedBy="enfantStructures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $structure;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $validation;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $slugEnfantStructure;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $ApplicationCreatedAt;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private $ResponseCreatedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $presence;


    public function getValidation(): ?string
    {
        return $this->validation;
    }

    public function setValidation(?string $validation): self
    {
        $this->validation = $validation;

        return $this;
    }

    public function getKid(): ?Enfant
    {
        return $this->kid;
    }

    public function setKid(?Enfant $kid): self
    {
        $this->kid = $kid;

        return $this;
    }

    public function getStructure(): ?Structure
    {
        return $this->structure;
    }

    public function setStructure(?Structure $structure): self
    {
        $this->structure = $structure;

        return $this;
    }

    public function getApplicationCreatedAt(): ?\DateTimeInterface
    {
        return $this->ApplicationCreatedAt;
    }

    public function setApplicationCreatedAt(\DateTimeInterface $ApplicationCreatedAt): self
    {
        $this->ApplicationCreatedAt = $ApplicationCreatedAt;

        return $this;
    }

    public function getResponseCreatedAt(): ?\DateTimeInterface
    {
        return $this->ResponseCreatedAt;
    }

    public function setResponseCreatedAt(\DateTimeInterface $ResponseCreatedAt): self
    {
        $this->ResponseCreatedAt = $ResponseCreatedAt;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getSlugEnfantStructure()
    {
        return $this->slugEnfantStructure;
    }

    /**
     * @param mixed $slugEnfantStructure
     */
    public function setSlugEnfantStructure($slugEnfantStructure): void
    {
        $this->slugEnfantStructure = $slugEnfantStructure;
    }

    public function createSlug (){
        return $this->getKid()->getSlug().'-' .$this->getStructure()->getSlugStructure();
    }

    public function getPresence(): ?bool
    {
        return $this->presence;
    }

    public function setPresence(?bool $presence): self
    {
        $this->presence = $presence;

        return $this;
    }


}
