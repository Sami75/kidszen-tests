<?php

namespace App\Entity;

use App\Entity\Traits\SluggbleTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EnfantRepository")
 * @ORM\Table(schema="kz")
 */
class Enfant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("enfant")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $birthday;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postalcode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $diet;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numsocialsecurity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numpublicliability;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $namepublicliability;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $imageright;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $outingright;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $midicalinfo;

    use SluggbleTrait;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Evenement", mappedBy="kidsevent")
     * @ORM\JoinTable(schema="kz")
     * @Groups("event_enfant")
     */
    private $events;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Personne", inversedBy="kids")
     * @ORM\JoinColumn(nullable=false)
     */
    private $family;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EnfantStructure", mappedBy="kid", orphanRemoval=true)
     */
    private $enfantStructures;

    public function __construct()
    {
        $this->events = new ArrayCollection();
        $this->enfantStructures = new ArrayCollection();
    }

    public function getAge()
    {
        $now = new \DateTime('now');
        $age = $this->getBirthday();
        $difference = $now->diff($age);

        return $difference->format('%y ans');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPostalcode(): ?string
    {
        return $this->postalcode;
    }

    public function setPostalcode(string $postalcode): self
    {
        $this->postalcode = $postalcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getDiet(): ?string
    {
        return $this->diet;
    }

    public function setDiet(?string $diet): self
    {
        $this->diet = $diet;

        return $this;
    }

    public function getNumsocialsecurity(): ?string
    {
        return $this->numsocialsecurity;
    }

    public function setNumsocialsecurity(?string $numsocialsecurity): self
    {
        $this->numsocialsecurity = $numsocialsecurity;

        return $this;
    }

    public function getNumpublicliability(): ?string
    {
        return $this->numpublicliability;
    }

    public function setNumpublicliability(?string $numpublicliability): self
    {
        $this->numpublicliability = $numpublicliability;

        return $this;
    }

    public function getNamepublicliability(): ?string
    {
        return $this->namepublicliability;
    }

    public function setNamepublicliability(?string $namepublicliability): self
    {
        $this->namepublicliability = $namepublicliability;

        return $this;
    }

    public function getImageright(): ?bool
    {
        return $this->imageright;
    }

    public function setImageright(?bool $imageright): self
    {
        $this->imageright = $imageright;

        return $this;
    }

    public function getOutingright(): ?bool
    {
        return $this->outingright;
    }

    public function setOutingright(?bool $outingright): self
    {
        $this->outingright = $outingright;

        return $this;
    }

    public function getMidicalinfo(): ?string
    {
        return $this->midicalinfo;
    }

    public function setMidicalinfo(?string $midicalinfo): self
    {
        $this->midicalinfo = $midicalinfo;

        return $this;
    }

    /**
     * @return Collection|Evenement[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Evenement $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->addKidsevent($this);
        }

        return $this;
    }

    public function removeEvent(Evenement $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            $event->removeKidsevent($this);
        }

        return $this;
    }

    public function getFamily(): ?Personne
    {
        return $this->family;
    }

    public function setFamily(?Personne $family): self
    {
        $this->family = $family;

        return $this;
    }

    /**
     * @return Collection|EnfantStructure[]
     */
    public function getEnfantStructures(): Collection
    {
        return $this->enfantStructures;
    }

    public function addEnfantStructure(EnfantStructure $enfantStructure): self
    {
        if (!$this->enfantStructures->contains($enfantStructure)) {
            $this->enfantStructures[] = $enfantStructure;
            $enfantStructure->setKid($this);
        }

        return $this;
    }

    public function removeEnfantStructure(EnfantStructure $enfantStructure): self
    {
        if ($this->enfantStructures->contains($enfantStructure)) {
            $this->enfantStructures->removeElement($enfantStructure);
            // set the owning side to null (unless already changed)
            if ($enfantStructure->getKid() === $this) {
                $enfantStructure->setKid(null);
            }
        }

        return $this;
    }
}
