<?php

namespace App\Entity;

use App\Entity\Traits\SluggbleTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonneRepository")
 * @UniqueEntity(
 *     fields={"email"},
 *    message="Cette adresse mail existe déja !"
 * )
 * @ORM\Table(schema="kz")
 */
class Personne implements UserInterface
{
    const GENDER = ['male' => 'm', 'femelle' => 'f', 'autre' => 'o'];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="2", minMessage="Votre prénom doit faire au minimum 2 charactères")
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="2", minMessage="Votre nom doit faire au minimum 2 charactères")
     */
    private $firstname;

    use SluggbleTrait;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private $birthday;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Regex(pattern="/^(0|\+33)[1-9]([-. ]?[0-9]{2}){4}$/", message="Numéro de téléphone incorrecte", groups={"complete"})
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string plainPassword
     * @Assert\NotBlank()
     * @Assert\Regex(groups={"resetPassword"}, pattern="/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*_])(?=.{8,})/",
     *      message="Minimum 8 caractères 1 lettre minuscule, 1 lettre majuscule, 1 chiffre et un caractère spécial.")
     */
    private $plainPassword;


    /**
     * @ORM\Column(type="boolean",options={"default":false})
     */
    private $accountconfirmation = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $accountconfirmationtoken;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tokenresetpassword;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private $tokenresetpasswordcreatedtat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Choice(choices=Personne::GENDER, groups={"complete"})
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $postalcode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $diploma;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $certification;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $townhall;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $townhallpostalcode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $townhallcity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $townhalldepartment;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $townhalltel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $townhallemail;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $activation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $situation;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $complete;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenement", mappedBy="creator")
     */
    private $eventscreated;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Evenement", mappedBy="eventresponsible")
     */
    private $events;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Enfant", mappedBy="family", orphanRemoval=true)
     */
    private $kids;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Structure", mappedBy="coordinator")
     */
    private $coordinatorstructures;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Structure", mappedBy="director", cascade={"persist", "remove"})
     */
    private $directorstructure;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Structure", mappedBy="animatorsstructure")
     */
    private $animatorstructures;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Actualite", mappedBy="author", orphanRemoval=true)
     */
    private $actualites;

    public function __construct()
    {
        $this->eventscreated = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->kids = new ArrayCollection();
        $this->coordinatorstructures = new ArrayCollection();
        $this->animatorstructures = new ArrayCollection();
        $this->actualites = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        //        $roles[] = 'ROLE_USER';
        //
        return array_unique($roles);
    }

    public function addRole($role)
    {
        $this->roles[] = $role;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): ?string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPostalcode(): ?string
    {
        return $this->postalcode;
    }

    public function setPostalcode(string $postalcode): self
    {
        $this->postalcode = $postalcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getDiploma(): ?string
    {
        return $this->diploma;
    }

    public function setDiploma(?string $diploma): self
    {
        $this->diploma = $diploma;

        return $this;
    }

    public function getCertification(): ?string
    {
        return $this->certification;
    }

    public function setCertification(?string $certification): self
    {
        $this->certification = $certification;

        return $this;
    }

    public function getTownhall(): ?string
    {
        return $this->townhall;
    }

    public function setTownhall(?string $townhall): self
    {
        $this->townhall = $townhall;

        return $this;
    }

    public function getTownhallpostalcode(): ?string
    {
        return $this->townhallpostalcode;
    }

    public function setTownhallpostalcode(?string $townhallpostalcode): self
    {
        $this->townhallpostalcode = $townhallpostalcode;

        return $this;
    }

    public function getTownhallcity(): ?string
    {
        return $this->townhallcity;
    }

    public function setTownhallcity(?string $townhallcity): self
    {
        $this->townhallcity = $townhallcity;

        return $this;
    }

    public function getTownhalldepartment(): ?string
    {
        return $this->townhalldepartment;
    }

    public function setTownhalldepartment(?string $townhalldepartment): self
    {
        $this->townhalldepartment = $townhalldepartment;

        return $this;
    }

    public function getTownhalltel(): ?string
    {
        return $this->townhalltel;
    }

    public function setTownhalltel(?string $townhalltel): self
    {
        $this->townhalltel = $townhalltel;

        return $this;
    }

    public function getTownhallemail(): ?string
    {
        return $this->townhallemail;
    }

    public function setTownhallemail(?string $townhallemail): self
    {
        $this->townhallemail = $townhallemail;

        return $this;
    }

    public function getActivation(): ?bool
    {
        return $this->activation;
    }

    public function setActivation(?bool $activation): self
    {
        $this->activation = $activation;

        return $this;
    }

    public function getSituation(): ?string
    {
        return $this->situation;
    }

    public function setSituation(?string $situation): self
    {
        $this->situation = $situation;

        return $this;
    }

    /**
     * @return Collection|Evenement[]
     */
    public function getEventscreated(): Collection
    {
        return $this->eventscreated;
    }

    public function addEventscreated(Evenement $eventscreated): self
    {
        if (!$this->eventscreated->contains($eventscreated)) {
            $this->eventscreated[] = $eventscreated;
            $eventscreated->setCreator($this);
        }

        return $this;
    }

    public function removeEventscreated(Evenement $eventscreated): self
    {
        if ($this->eventscreated->contains($eventscreated)) {
            $this->eventscreated->removeElement($eventscreated);
            // set the owning side to null (unless already changed)
            if ($eventscreated->getCreator() === $this) {
                $eventscreated->setCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Evenement[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Evenement $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->addEventresponsible($this);
        }

        return $this;
    }

    public function removeEvent(Evenement $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            $event->removeEventresponsible($this);
        }

        return $this;
    }

    /**
     * @return Collection|Enfant[]
     */
    public function getKids(): Collection
    {
        return $this->kids;
    }

    public function addKid(Enfant $kid): self
    {
        if (!$this->kids->contains($kid)) {
            $this->kids[] = $kid;
            $kid->setFamily($this);
        }

        return $this;
    }

    public function removeKid(Enfant $kid): self
    {
        if ($this->kids->contains($kid)) {
            $this->kids->removeElement($kid);
            // set the owning side to null (unless already changed)
            if ($kid->getFamily() === $this) {
                $kid->setFamily(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Structure[]
     */
    public function getCoordinatorstructures(): Collection
    {
        return $this->coordinatorstructures;
    }

    public function addCoordinatorstructure(Structure $coordinatorstructure): self
    {
        if (!$this->coordinatorstructures->contains($coordinatorstructure)) {
            $this->coordinatorstructures[] = $coordinatorstructure;
            $coordinatorstructure->setCoordinator($this);
        }

        return $this;
    }

    public function removeCoordinatorstructure(Structure $coordinatorstructure): self
    {
        if ($this->coordinatorstructures->contains($coordinatorstructure)) {
            $this->coordinatorstructures->removeElement($coordinatorstructure);
            // set the owning side to null (unless already changed)
            if ($coordinatorstructure->getCoordinator() === $this) {
                $coordinatorstructure->setCoordinator(null);
            }
        }

        return $this;
    }

    public function getDirectorstructure(): ?Structure
    {
        return $this->directorstructure;
    }

    public function setDirectorstructure(?Structure $directorstructure): self
    {
        $this->directorstructure = $directorstructure;

        // set (or unset) the owning side of the relation if necessary
        $newDirector = null === $directorstructure ? null : $this;
        if ($directorstructure->getDirector() !== $newDirector) {
            $directorstructure->setDirector($newDirector);
        }

        return $this;
    }

    /**
     * @return Collection|Structure[]
     */
    public function getAnimatorstructures(): Collection
    {
        return $this->animatorstructures;
    }

    public function addAnimatorstructure(Structure $animatorstructure): self
    {
        if (!$this->animatorstructures->contains($animatorstructure)) {
            $this->animatorstructures[] = $animatorstructure;
            $animatorstructure->addAnimatorsstructure($this);
        }

        return $this;
    }

    public function removeAnimatorstructure(Structure $animatorstructure): self
    {
        if ($this->animatorstructures->contains($animatorstructure)) {
            $this->animatorstructures->removeElement($animatorstructure);
            $animatorstructure->removeAnimatorsstructure($this);
        }

        return $this;
    }

    /**
     * @return Collection|Actualite[]
     */
    public function getActualites(): Collection
    {
        return $this->actualites;
    }

    public function addActualite(Actualite $actualite): self
    {
        if (!$this->actualites->contains($actualite)) {
            $this->actualites[] = $actualite;
            $actualite->setAuthor($this);
        }

        return $this;
    }

    public function removeActualite(Actualite $actualite): self
    {
        if ($this->actualites->contains($actualite)) {
            $this->actualites->removeElement($actualite);
            // set the owning side to null (unless already changed)
            if ($actualite->getAuthor() === $this) {
                $actualite->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccountconfirmation()
    {
        return $this->accountconfirmation;
    }

    /**
     * @param mixed $accountconfirmation
     */
    public function setAccountconfirmation($accountconfirmation): void
    {
        $this->accountconfirmation = $accountconfirmation;
    }

    /**
     * @return mixed
     */
    public function getAccountconfirmationtoken()
    {
        return $this->accountconfirmationtoken;
    }

    /**
     * @param mixed $accountconfirmationtoken
     */
    public function setAccountconfirmationtoken($accountconfirmationtoken): void
    {
        $this->accountconfirmationtoken = $accountconfirmationtoken;
    }

    /**
     * @return mixed
     */
    public function getTokenresetpassword()
    {
        return $this->tokenresetpassword;
    }

    /**
     * @param mixed $tokenresetpassword
     */
    public function setTokenresetpassword($tokenresetpassword): void
    {
        $this->tokenresetpassword = $tokenresetpassword;
    }

    /**
     * @return mixed
     */
    public function getTokenresetpasswordcreatedtat()
    {
        return $this->tokenresetpasswordcreatedtat;
    }

    /**
     * @param mixed $tokenresetpasswordcreatedtat
     */
    public function setTokenresetpasswordcreatedtat($tokenresetpasswordcreatedtat): void
    {
        $this->tokenresetpasswordcreatedtat = $tokenresetpasswordcreatedtat;
    }


    /**
     * @return string
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @return mixed
     */
    public function getComplete()
    {
        return $this->complete;
    }

    /**
     * @param mixed $complete
     */
    public function setComplete($complete): void
    {
        $this->complete = $complete;
    }



    /**
     * @param string $plainPassword
     */
    public function setPlainPassword(string $plainPassword)
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    public function getFullName()
    {
        return $this->getFirstname() . ' ' . $this->getLastname();
    }

    /**
     * @param array $roles
     */
    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }



    public function isAuthor($roles)
    {
        foreach ($roles as $role) {
            switch ($role) {
                case "ROLE_ADMIN":
                case "ROLE_COORDINATEUR":
                case "ROLE_DIRECTEUR":
                case "ROLE_ANIMATEUR":
                    return true;
                case "ROLE_FAMILLE":
                default:
                    return false;
            }
        };
    }

    public function __toString()
    {
        return $this->getFullName();
    }

    public function convertGender()
    {
        if($this->gender === 'm') {
            return 'male';
        } else {
            return 'female';
        }
    }
}
