<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EvenementRepository")
 * @ORM\Table(schema="kz")
 */
class Evenement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private $startdate;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private $enddate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $duration;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $mbmaxchildren;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\EmploiTemps", mappedBy="evenements")
     */
    private $emploiTemps;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Personne", inversedBy="eventscreated")
     * @ORM\JoinColumn(nullable=false)
     */
    private $creator;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Personne", inversedBy="events")
     * @ORM\JoinTable(schema="kz")
     */
    private $eventresponsible;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Enfant", inversedBy="events")
     * @ORM\JoinTable(schema="kz")
     */
    private $kidsevent;

    private $update;

    public function __construct()
    {
        $this->emploiTemps = new ArrayCollection();
        $this->eventresponsible = new ArrayCollection();
        $this->kidsevent = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStartdate(): ?\DateTimeInterface
    {
        return $this->startdate;
    }

    public function setStartdate(?\DateTimeInterface $startdate): self
    {
        $this->startdate = $startdate;

        return $this;
    }

    public function getEnddate(): ?\DateTimeInterface
    {
        return $this->enddate;
    }

    public function setEnddate(?\DateTimeInterface $enddate): self
    {
        $this->enddate = $enddate;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(?string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getMbmaxchildren(): ?int
    {
        return $this->mbmaxchildren;
    }

    public function setMbmaxchildren(?int $mbmaxchildren): self
    {
        $this->mbmaxchildren = $mbmaxchildren;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|EmploiTemps[]
     */
    public function getEmploiTemps(): Collection
    {
        return $this->emploiTemps;
    }

    public function addEmploiTemp(EmploiTemps $emploiTemp): self
    {
        if (!$this->emploiTemps->contains($emploiTemp)) {
            $this->emploiTemps[] = $emploiTemp;
            $emploiTemp->addEvenement($this);
        }

        return $this;
    }

    public function removeEmploiTemp(EmploiTemps $emploiTemp): self
    {
        if ($this->emploiTemps->contains($emploiTemp)) {
            $this->emploiTemps->removeElement($emploiTemp);
            $emploiTemp->removeEvenement($this);
        }

        return $this;
    }

    public function getCreator(): ?Personne
    {
        return $this->creator;
    }

    public function setCreator(?Personne $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getEventresponsible(): Collection
    {
        return $this->eventresponsible;
    }

    public function addEventresponsible(Personne $eventresponsible): self
    {
        if (!$this->eventresponsible->contains($eventresponsible)) {
            $this->eventresponsible[] = $eventresponsible;
        }

        return $this;
    }

    public function removeEventresponsible(Personne $eventresponsible): self
    {
        if ($this->eventresponsible->contains($eventresponsible)) {
            $this->eventresponsible->removeElement($eventresponsible);
        }

        return $this;
    }

    /**
     * @return Collection|Enfant[]
     */
    public function getKidsevent(): Collection
    {
        return $this->kidsevent;
    }

    public function addKidsevent(Enfant $kidsevent): self
    {
        if (!$this->kidsevent->contains($kidsevent)) {
            $this->kidsevent[] = $kidsevent;
        }

        return $this;
    }

    public function removeKidsevent(Enfant $kidsevent): self
    {
        if ($this->kidsevent->contains($kidsevent)) {
            $this->kidsevent->removeElement($kidsevent);
        }

        return $this;
    }

    public function getUpdate()
    {
        return $this->update;
    }

    public function setUpdate(string $value)
    {
        $this->update = $value;

        return $this;
    }

    public function checkDates($startDate, $endDate)
    {
        return $startDate <= $endDate;
    }
}
