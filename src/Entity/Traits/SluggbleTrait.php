<?php


namespace App\Entity\Traits;

use Gedmo\Mapping\Annotation as Gedmo;

trait SluggbleTrait
{

    /**
     * @ORM\Column(type="string", unique=true)
     * @Gedmo\Slug(fields={"lastname", "firstname"})
     */
    private $slug;

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }
}
